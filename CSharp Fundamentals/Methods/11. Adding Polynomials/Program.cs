﻿using System;
using System.Linq;

namespace _11._Adding_Polynomials
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            int[] numbers = new int[n];

            for (int i = 0; i < 2; i++)
            {
                int[] currNums = ReadArrayNumbers();
                for (int j = 0; j < n; j++)
                {
                    numbers[j] += currNums[j];
                }
            }

            foreach (var num in numbers)
            {
                Console.Write(num + " ");
            }
        }

        static int[] ReadArrayNumbers()
        {
            int[] numbers = Console.ReadLine().Split().Select(int.Parse).ToArray();

            return numbers;
        }
    }
}
