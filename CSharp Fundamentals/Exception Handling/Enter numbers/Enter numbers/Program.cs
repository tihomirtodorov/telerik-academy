﻿using System;
using System.Text;

namespace Enter_numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int numbersCount = 10;

            StringBuilder sb = new StringBuilder();

            try
            {
                int lastNumber = ReadNumber(1, 100);
                sb.Append("1 < ").AppendFormat($"{lastNumber} < ");

                for (int i = 0; i < numbersCount - 1; i++)
                {
                    int currNumber = ReadNumber(lastNumber, 100);
                    sb.AppendFormat($"{lastNumber} < ");
                    lastNumber = currNumber;
                }

                sb.Append("100");

                Console.WriteLine(sb.ToString().TrimEnd());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
               
            }
        }

        public static int ReadNumber(int start, int end)
        {
            int number = int.Parse(Console.ReadLine());

            if(number > start && number < end)
            {
                return number;
            }
            else
            {
                throw new ArgumentException("Exception");
            }
        }
        
    }
}
