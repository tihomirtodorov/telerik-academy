﻿using System;
using System.Linq;

namespace Challenge_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = "git is a VERSION CONTROL system for tracking changes in COMPUTER FILES " +
                "and coordinating work on those files among multiple people";
            
            char[] charArray = input.ToLower().ToCharArray().Distinct().ToArray();

            foreach (var symbol in charArray)
            {
                if (!char.IsLetter(symbol))
                {
                    continue;
                }
                Console.Write(symbol);
            }
            Console.WriteLine();
        }
    }
}
