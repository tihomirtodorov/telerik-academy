﻿using System;
using System.Linq;

namespace ConsoleApp1
{
    class Challenge_1
    {
        static void Main(string[] args)
        {
            string input = "git is a <<version control>> system for tracking changes in <<computer files>> " +
                "and coordinating work on those files among multiple <<people>>";

            bool isFound = true;

            int signSize = 2;

            while (isFound != false)
            {
                int startIndex = input.IndexOf('<');
                int endIndex = input.IndexOf('>');

                if (startIndex < 0 || endIndex < 0)
                {
                    isFound = false;
                    break;
                }


                string substring = input.Substring(startIndex + signSize, endIndex - startIndex - signSize);

                input = input.Remove(startIndex, signSize);
                input = input.Remove(endIndex - signSize, signSize);

                input = input.Replace(substring, substring.ToUpper());
            }

            Console.WriteLine(input);
        }

    }
}
