﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Hops
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = Console.ReadLine().Split(new char[] {' ', ','}, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();

            bool[] visitedArr = new bool[arr.Length];
            int amountOfDirections = int.Parse(Console.ReadLine());

            int maxSum = int.MinValue;

            for (int i = 0; i < amountOfDirections; i++)
            {

                int[] currDirections = Console.ReadLine().Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();

                ResetVisitedIndexes(visitedArr);

                int currIndex = 0;
                int currentSum = 0;
                int step = 0;
                while(currIndex >= 0 && currIndex < arr.Length && visitedArr[currIndex] == false)
                {
                    currentSum += arr[currIndex];
                    visitedArr[currIndex] = true;
                    currIndex += currDirections[step];
                    step++;
                    if(step >= currDirections.Length)
                    {
                        step = 0;
                    }
                }


                if (currentSum > maxSum)
                {
                    maxSum = currentSum;
                }
            }

            Console.WriteLine(maxSum);
        }

        private static void ResetVisitedIndexes(bool[] visitedArr)
        {
            for (int i = 0; i < visitedArr.Length; i++)
            {
                visitedArr[i] = false;
            }
        }
    }
}
