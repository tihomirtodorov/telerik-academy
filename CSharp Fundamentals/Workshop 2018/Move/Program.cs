﻿using System;
using System.Linq;
using System.Numerics;

namespace Move
{
    class Program
    {
        static void Main(string[] args)
        {
            int backwardMoves = 0;
            int forwardMoves = 0;

            int startPoint = int.Parse(Console.ReadLine());
            int[] numbers = Console.ReadLine().Split(new char[] { ',' }).Select(int.Parse).ToArray();

            int currentIndex = startPoint;

            while (true)
            {
                string[] cmdArgs = Console.ReadLine().Split().ToArray();
                if (cmdArgs[0] == "exit")
                {
                    break;
                }

                int steps = int.Parse(cmdArgs[0]);
                string commandType = cmdArgs[1];
                int size = int.Parse(cmdArgs[2]);

                if (commandType == "forward")
                {
                    for (int i = 0; i < steps; i++)
                    {
                        currentIndex = currentIndex + size;

                        currentIndex = currentIndex % numbers.Length;

                        forwardMoves += numbers[currentIndex];
                    }

                }
                else if (commandType == "backwards")
                {
                    for (int i = steps - 1; i >= 0; i--)
                    {
                        currentIndex = currentIndex - size;

                        currentIndex = currentIndex % numbers.Length;

                        if(currentIndex < 0)
                        {
                            int stepsToGoBack = currentIndex;
                            currentIndex = 0;
                            currentIndex = numbers.Length + stepsToGoBack;
                        }

                        backwardMoves += numbers[currentIndex];
                    }

                }


            }

            Console.WriteLine($"Forward: {forwardMoves}");
            Console.WriteLine($"Backwards: {backwardMoves}");
        }
    }
}
