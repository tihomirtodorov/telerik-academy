﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Third
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] dimensions = Console.ReadLine().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();

            int matrixRows = dimensions[0];
            int matrixCols = dimensions[1];
            int jumpsCount = dimensions[2];

            int[] startIndexes = Console.ReadLine().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
            int startRow = startIndexes[0];
            int startCol = startIndexes[1];

            int[,] matrix = new int[matrixRows, matrixCols];

            List<string> indexes = new List<string>();

            BigInteger sum = 0;
            int numberOfJumps = 0;

            for (int i = 0; i < jumpsCount; i++) // save all jump indexes;
            {
                int[] currentIndexes = Console.ReadLine().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();

                string rowAndCol = currentIndexes[0] + "," + currentIndexes[1].ToString();
                indexes.Add(rowAndCol);
            }

            int counter = 0;

            for (int rows = 0; rows < matrixRows; rows++)
            {
                for (int cols = 0; cols < matrixCols; cols++)
                {
                    matrix[rows, cols] = ++counter;
                }
            } // Fill matrix;

            for (int i = 0; i < indexes.Count; i++)
            {
                string[] rowAndCol = indexes[i].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                string charRow = rowAndCol[0];
                string charCol = rowAndCol[1];

                int nextRow = int.Parse(charRow.ToString());
                int nextCol = int.Parse(charCol.ToString());

                sum += matrix[startRow, startCol];
                matrix[startRow, startCol] = 0;

                startRow += nextRow;
                startCol += nextCol;

                if (startRow < 0 || startRow >= matrixRows || startCol < 0 || startCol >= matrixCols)
                {
                    Console.WriteLine("escaped " + sum);
                    return;
                }

                //numberOfJumps++;
                ////if (matrix[startRow, startCol] == 0)
                ////{
                ////    Console.WriteLine("caught " + numberOfJumps);
                ////    return;
                ////}

                if (i == indexes.Count - 1)
                {
                    i = -1;
                }
            }
        }
    }
}
