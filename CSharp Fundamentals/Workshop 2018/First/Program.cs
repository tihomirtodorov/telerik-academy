﻿using System;
using System.Linq;
using System.Text;

namespace First
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.Write("Decimal: ");
            //int decimalNumber = int.Parse(Console.ReadLine());

            //int remainder;
            //string result = string.Empty;
            //while (decimalNumber > 0)
            //{
            //    remainder = decimalNumber % 2;
            //    decimalNumber /= 2;
            //    result = remainder.ToString() + result;
            //}
            //Console.WriteLine("Binary:  {0}",result);

            //28 = 00011100
            //1 = 00000001
            //45 = 00101101
            //255 = 11111111


            int[] numbers = Console.ReadLine().Split(new char[] { ',' }).Select(int.Parse).ToArray();

            StringBuilder sb = new StringBuilder();




            //28 = 00011100
            //1 = 00000001
            //45 = 00101101
            //255 = 11111111

            for (int i = 0; i < numbers.Length; i++)
            {
                int value = numbers[i];
                string binary = Convert.ToString(value, 2);
                var newString = binary.PadLeft(8, '0');

                string result = string.Empty;

                if (i % 2 == 0)
                {
                    for (int j = 0; j < newString.Length; j++)
                    {
                        if(j % 2 != 0)
                        {
                            result += newString[j];
                        }
                    }
                    sb.Append(result);
                }
                else if (i % 2 != 0)
                {
                    for (int j = 0; j < newString.Length; j++)
                    {
                        if (j % 2 == 0)
                        {
                            result += newString[j];
                        }
                    }
                    sb.Append(result);
                }
            }

            Console.WriteLine(sb.ToString().TrimEnd());
        }
    }
}
