﻿using System;
using System.Linq;
using System.Numerics;

namespace Bounce
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] inputParams = Console.ReadLine().Split().Select(int.Parse).ToArray();
            int rowsDimension = inputParams[0];
            int colsDimension = inputParams[1];

            BigInteger[,] matrix = new BigInteger[rowsDimension, colsDimension];

            BigInteger sum = 0;

            int rowCurrent = 0;
            int colCurrent = 0;

            int rowChange = 1;
            int colChange = 1;

            for (int row = 0; row < rowsDimension; row++)
            {
                for (int col = 0; col < colsDimension; col++)
                {
                    matrix[row, col] = FillRow(matrix, row, col);
                }
            } // Fill matrix;

            if(rowsDimension == 1 || colsDimension == 1)
            {
                Console.WriteLine(1);
                return;
            }

            while (true)
            {
                sum += matrix[rowCurrent, colCurrent];

                rowCurrent += rowChange;
                colCurrent += colChange;

                if (rowCurrent == rowsDimension - 1) // going top max
                {
                    rowChange = -1;
                }
                else if (colCurrent == colsDimension - 1) // going to the right max
                {
                    colChange = -1;
                }
                else if (rowCurrent == 0) // going bottom max
                {
                    rowChange = 1;
                }
                else if (colCurrent == 0) // going left max // TO DO CHANGE IT;
                {
                    colChange = 1;
                }

                if ((rowCurrent == 0 && colCurrent == 0)
                        || (rowCurrent == 0 && colCurrent == colsDimension - 1)
                        || (rowCurrent == rowsDimension - 1 && colCurrent == 0)
                        || (rowCurrent == rowsDimension - 1 && colCurrent == colsDimension - 1))
                {
                    sum += matrix[rowCurrent, colCurrent];

                    break;
                }

            }

            Console.WriteLine(sum);
        }

        static BigInteger FillRow(BigInteger[,] matrix, int row, int col)
        {
            BigInteger value = BigInteger.Pow(2, (0 + row + col));
            return (BigInteger)value;
        }
    }
}
