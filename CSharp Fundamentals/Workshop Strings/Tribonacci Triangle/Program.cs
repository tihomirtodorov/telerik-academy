﻿using System;
using System.Numerics;

namespace Tribonacci_Triangle
{
    class Program
    {
        static void Main(string[] args)
        {
            BigInteger firstNum = Convert.ToInt32(Console.ReadLine());
            BigInteger secondNum = Convert.ToInt32(Console.ReadLine());
            BigInteger thirdNum = Convert.ToInt32(Console.ReadLine());
            int numberOfLines = int.Parse(Console.ReadLine());


            Console.WriteLine(firstNum);
            Console.WriteLine(secondNum + " " + thirdNum);

            BigInteger result;

            for (int i = 3; i <= numberOfLines; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    result = firstNum + secondNum + thirdNum;
                    firstNum = secondNum;
                    secondNum = thirdNum;
                    thirdNum = result;

                    Console.Write(result + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
