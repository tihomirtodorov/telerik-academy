﻿using System;
using System.Linq;

namespace Subset_of_Sum_S
{
    class Program
    {
        static void Main(string[] args)
        {
            int wantedSum = int.Parse(Console.ReadLine());
            int[] numbers = Console.ReadLine().Split().Select(int.Parse).ToArray();

            for (int i = 0; i < numbers.Length; i++)
            {
                int currentSum = numbers[i];

                for (int j = i + 1; j < numbers.Length; j++)
                {
                    int nextNum = numbers[j];
                    if (currentSum + nextNum <= wantedSum)
                    {
                        currentSum += nextNum;
                    }

                    if (currentSum == wantedSum)
                    {
                        Console.WriteLine("yes");
                        return;
                    }
                }
            }

            Console.WriteLine("no");
        }
    }
}