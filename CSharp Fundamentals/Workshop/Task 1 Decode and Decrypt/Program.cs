﻿using System;

namespace Task_1_Decode_and_Decrypt
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputMessage = "ABBAA6BA7";

            string cypherLength = GetCypherLength(inputMessage);
            string inputWithoutLength = RemovedLengthOfCypher(inputMessage, cypherLength);

            string decoded = Decode(inputWithoutLength);

            string cypher = GetCypher(decoded, cypherLength);

            string message = GetMessage(decoded, cypher);

            string decrypted = Encrypt(message, cypher);

            Console.WriteLine(decrypted);

        }

        static string GetCypherLength(string inputMessage)
        {
            string result = string.Empty;
            int i = inputMessage.Length - 1;

            while (char.IsDigit(inputMessage[i]))
            {
                result = inputMessage[i] + result;
                i--;
            }

            return result;
        }

        static string RemovedLengthOfCypher(string all, string lengthOfCypher)
        {
            var withoutLength = all.Length - lengthOfCypher.Length;

            return all.Substring(0, withoutLength);
        }

        static string Decode(string encodedMsg)
        {
            //  the text "aaaabbbccccaa" we will have "4a3b4caa".
            string result = string.Empty;

            for (int i = 0; i < encodedMsg.Length; i++)
            {
                string numbers = string.Empty;
                while(char.IsDigit(encodedMsg[i]) == true)
                {
                    numbers += encodedMsg[i];
                    i++;
                }

                char symbol = encodedMsg[i];

                if (string.IsNullOrEmpty(numbers))
                {
                    numbers = "1";
                }

                int timesToRepeat = int.Parse(numbers);

                result += new string(symbol, timesToRepeat);
            }

            return result;
        }

        static string GetCypher(string fullMsgAndCypher, string cypherLength)
        {
            int parsedCypherLength = int.Parse(cypherLength);

            string result = fullMsgAndCypher.Substring(fullMsgAndCypher.Length - parsedCypherLength);

            return result;
        }

        static string GetMessage(string fullMsgAndCypher, string cypher)
        {
            string result = fullMsgAndCypher.Substring(0, fullMsgAndCypher.Length - cypher.Length);

            return result;
        }

        static string Encrypt(string message, string cypher)
        {
            char[] result = message.ToCharArray();
            var length = Math.Max(message.Length, cypher.Length);

            for (int i = 0; i < length; i++)
            {
                int first = CodeOf(result[i % result.Length]);
                int second = CodeOf(cypher[i % cypher.Length]);
                var newCode = CodeOf(result[i % result.Length]) ^ CodeOf(cypher[i % cypher.Length]);
                var newChar = CharOf(newCode);

                result[i % result.Length] = newChar;
            }

            return new string(result);
        }

        static int CodeOf(char symbol)
        {
            return symbol - 'A';
        }

        static char CharOf(int code)
        {
            return (char)(code + 'A');
        }
    }
}
