﻿using System;
using System.Linq;
using System.Numerics;

namespace Bit_Shift_Matrix
{
    class Program
    {
        static void Main(string[] args)

        {
            int rowsDimension = int.Parse(Console.ReadLine());
            int colsDimension = int.Parse(Console.ReadLine());

            BigInteger[,] matrix = new BigInteger[rowsDimension, colsDimension];

            int movesCount = int.Parse(Console.ReadLine());

            int[] numbers = Console.ReadLine().Split().Select(int.Parse).ToArray(); // MIGHT BE DECIMAL;

            BigInteger sum = 0;

            int rowCurrPawn = rowsDimension - 1;
            int colCurrPawn = 0;

            for (int row = 0; row < rowsDimension; row++)
            {
                for (int col = 0; col < colsDimension; col++)
                {
                    matrix[row, col] = FillRow(matrix, row, col);
                }
            }

            int coeff = Math.Max(rowsDimension, colsDimension);

            for (int i = 0; i < numbers.Length; i++)
            {
                int endRow = numbers[i] / coeff;

                int endCol = numbers[i] % coeff;

                int index = Math.Abs(colCurrPawn - endCol); // used to check how many times we have to iterate.

                // column move calculations
                for (int j = 0; j < index; j++)
                {
                    if (colCurrPawn <= endCol)
                    {
                        sum += matrix[rowCurrPawn, colCurrPawn];
                        matrix[rowCurrPawn, colCurrPawn] = 0;

                        colCurrPawn++;
                    }
                    else
                    {
                        sum += matrix[rowCurrPawn, colCurrPawn];
                        matrix[rowCurrPawn, colCurrPawn] = 0;

                        colCurrPawn--;
                    }
                }

                index = Math.Abs(rowCurrPawn - endRow);

                // row move calculations
                for (int j = 0; j <= index; j++)
                {
                    if (rowCurrPawn < endRow)
                    {
                        sum += matrix[rowCurrPawn, endCol];
                        matrix[rowCurrPawn, endCol] = 0;

                        rowCurrPawn++;
                    }
                    else
                    {
                        sum += matrix[rowCurrPawn, endCol];
                        matrix[rowCurrPawn, endCol] = 0;

                        rowCurrPawn--;
                    }
                }

                rowCurrPawn = endRow;
                colCurrPawn = endCol;

            }
            Console.WriteLine(sum);

        }



        static BigInteger FillRow(BigInteger[,] matrix, int row, int col)
        {
            BigInteger value = BigInteger.Pow(2, (matrix.GetLength(0) - 1 - row + col));
            return value;
        }
    }
}
