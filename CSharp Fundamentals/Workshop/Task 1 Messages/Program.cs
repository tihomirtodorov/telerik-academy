﻿using System;

namespace Task_1_Messages
{
    class Program
    {
        static void Main(string[] args)
        {
            string firstNumString = Console.ReadLine();
            char operation = char.Parse(Console.ReadLine());
            string secondNumString = Console.ReadLine();

            long firstSum = 0;
            long secondSum = 0;

            firstSum = GetCharValue(firstNumString, firstSum);
            secondSum = GetCharValue(secondNumString, secondSum);

            long value = operation == '+' ? firstSum + secondSum : firstSum - secondSum;

            Console.WriteLine(GetResultString(value.ToString()));
        }

        private static long GetCharValue(string inputString, long sum)
        {
            string currentSum = string.Empty;
            for (int i = 0; i < inputString.Length; i += 3)
            {
                string currentChars = inputString.Substring(i, 3);

                currentSum = currentChars == "cad" ? currentSum += 0 : currentSum;
                currentSum = currentChars == "xoz" ? currentSum += 1 : currentSum;
                currentSum = currentChars == "nop" ? currentSum += 2 : currentSum;
                currentSum = currentChars == "cyk" ? currentSum += 3 : currentSum;
                currentSum = currentChars == "min" ? currentSum += 4 : currentSum;

                currentSum = currentChars == "mar" ? currentSum += 5 : currentSum;
                currentSum = currentChars == "kon" ? currentSum += 6 : currentSum;
                currentSum = currentChars == "iva" ? currentSum += 7 : currentSum;
                currentSum = currentChars == "ogi" ? currentSum += 8 : currentSum;
                currentSum = currentChars == "yan" ? currentSum += 9 : currentSum;
            }

            long parsedSum = long.Parse(currentSum);
            return parsedSum;
        }

        private static string GetResultString(string numbers)
        {
            string currString = string.Empty;
            for (int i = 0; i < numbers.Length; i++)
            {
                char currChar = numbers[i];

                currString = currChar == '0' ? currString += "cad" : currString;
                currString = currChar == '1' ? currString += "xoz" : currString;
                currString = currChar == '2' ? currString += "nop" : currString;
                currString = currChar == '3' ? currString += "cyk" : currString;
                currString = currChar == '4' ? currString += "min" : currString;

                currString = currChar == '5' ? currString += "mar" : currString;
                currString = currChar == '6' ? currString += "kon" : currString;
                currString = currChar == '7' ? currString += "iva" : currString;
                currString = currChar == '8' ? currString += "ogi" : currString;
                currString = currChar == '9' ? currString += "yan" : currString;
            }

            return currString;
        }
    }
}
