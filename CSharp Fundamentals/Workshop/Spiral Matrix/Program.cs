﻿using System;

namespace Spiral_Matrix
{
    class Program
    {
        static void Main(string[] args)
        {
            int size = int.Parse(Console.ReadLine());

            int[,] matrix = new int[size, size];

            int value = 0;

            int startCol = 0;

            int endCol = size - 1;

            int startRow = 0;

            int endRow = size - 1;

            while(startRow <= endRow && startCol <= endCol)
            {
                for (int i = startCol; i <= endCol; i++)
                {
                    matrix[startRow, i] = ++value;
                }

                for (int i = startRow + 1; i <= endRow; i++)
                {
                    matrix[i, endCol] = ++value;
                }

                for (int i = endCol - 1; i >= startCol; i--)
                {
                    matrix[endRow, i] = ++value;
                }

                for (int i = endRow - 1; i >= startRow + 1; i--)
                {
                    matrix[i, startCol] = ++value;

                }

                startRow++;
                startCol++;

                endRow--;
                endCol--;
            }

            for (int row = 0; row < size; row++)
            {
                for (int col = 0; col < size; col++)
                {
                    Console.Write(matrix[row, col] + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
