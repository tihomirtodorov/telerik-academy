﻿using System;
using System.Collections.Generic;

namespace Longest_Sequence_of_Equal
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            List<int> numbers = new List<int>();

            int maxSequence = 0;

            for (int i = 0; i < n; i++)
            {
                int currNum = int.Parse(Console.ReadLine());

                numbers.Add(currNum);
            }

            for (int i = 0; i < numbers.Count; i++)
            {
                int currNum = numbers[i];

                int currentSeqCount = 1;

                for (int j = i + 1; j < numbers.Count; j++)
                {
                    int nextNum = numbers[j];

                    if(currNum == nextNum)
                    {
                        currentSeqCount++;

                        if(currentSeqCount > maxSequence)
                        {
                            maxSequence = currentSeqCount;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            Console.WriteLine(maxSequence);
        }
    }
}
