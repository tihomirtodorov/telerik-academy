﻿using System;

namespace Jump_Jump
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = Console.ReadLine();

            int index = 0;

            while(index < input.Length)
            {
                char currChar = input[index];
                int number = int.Parse(currChar.ToString());


                if (currChar == '^')
                {
                    Console.WriteLine($"Jump, Jump, DJ Tomekk kommt at {index}");
                    return;
                }
                else if (number == 0)
                {
                    Console.WriteLine($"Too drunk to go on after {index}");
                    return;
                }
                else if (number % 2 == 0)
                {
                    index += number;
                }
                else if (number % 2 != 0)
                {
                    index -= number;

                }

                
                if(index >= input.Length || index < 0)
                {
                    Console.WriteLine($"Fell off the dancefloor at {index}");
                    return;
                }
            }
        }
    }
}
