﻿using System;

namespace Mythical_Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int inputNum = int.Parse(Console.ReadLine());

            int thirdDigit = inputNum % 10;
            inputNum = inputNum / 10;
            int secondNum = inputNum % 10;
            inputNum = inputNum / 10;
            int firstNum = inputNum % 10;
            inputNum = inputNum / 10;

            double result = 0;

            if (thirdDigit == 0)
            {
                result = firstNum * secondNum;
            }
            else if (thirdDigit >= 0 && thirdDigit <= 5)
            {
                result = firstNum * secondNum;
                result = result / thirdDigit;
            }
            else if (thirdDigit > 5)
            {
                result = firstNum * secondNum;
                result = result * thirdDigit;
            }

            Console.WriteLine($"{result:f2}");
        }
    }
}
