﻿using GSM_System.Models;
using GSM_System.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace GSM_System
{
    public static class CallHistoryTest
    {
        private const decimal CostPerMinute = 0.32m;

        public static void ShowHistoryTest()
        {
            // Create phone;
            GSM samsung = new GSM("Samsung S8", "Samsung", 1000);

            // Create call and add it afterwards through the first ctor;
            Calls call = new Calls(new DateTime(2019, 12, 30), "09461596512", 450);

            // Add the call through the second ctor;
            samsung.AddCall(new DateTime(2015, 04, 20), "2131231231", 300);
            samsung.AddCall(call);

            // Show history with the two calls;
            Console.WriteLine(samsung.CallHistory);
            Console.WriteLine();

            // Calculate the total calls price;
            Console.WriteLine(samsung.TotalCallsPrice(CostPerMinute));

            // Clear history and show it afterwards;
            samsung.ClearHistory();
            Console.WriteLine();
            Console.WriteLine(samsung.CallHistory);
        }


    }
}
