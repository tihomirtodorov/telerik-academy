﻿using GSM_System.Models.Contracts;
using System;
using System.Text;

namespace GSM_System.Models
{
    public class Calls : ICalls
    {
        private DateTime date;
        private int duration;
        private string dialledPhone;

        public Calls(DateTime date, string dialledPhone, int duration)
        {
            this.Date = date;
            this.DialledPhone = dialledPhone;
            this.Duration = duration;
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public string DialledPhone
        {
            get { return dialledPhone; }
            set { dialledPhone = value; }
        }

        public int Duration
        {
            get { return duration; }
            set { duration = value; }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"Date: {this.Date}, Number: {this.DialledPhone}, Duration: {this.Duration}");

            return sb.ToString().TrimEnd();
        }
    }
}