﻿using GSM_System.Models.Contracts;
using System.Text;

namespace GSM_System.Models
{
    public class Battery : IBattery
    {
        private BatteryType? batteryType;
        private double? hoursIdle;
        private double? hoursTalk;

        public Battery(BatteryType? batteryType = null, double? hoursIdle = null, double? hoursTalk = null)
        {
            this.BatteryType = batteryType;
            this.HoursIdle = hoursIdle;
            this.HoursTalk = hoursTalk;
        }

        // TODO validate properties
        public double? HoursIdle
        {
            get { return hoursIdle; }
            set { hoursIdle = value; }
        }

        public double? HoursTalk
        {
            get { return hoursTalk; }
            set { hoursTalk = value; }
        }

        public BatteryType? BatteryType
        {
            get { return batteryType; }
            set { batteryType = value; }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Battery Type: {this.BatteryType}, Hours Idle: {this.HoursIdle}, Hours Talk: {this.HoursTalk}");

            return sb.ToString().TrimEnd();
        }
    }
}