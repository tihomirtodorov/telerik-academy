﻿using System;

namespace GSM_System.Models.Contracts
{
    public interface ICalls
    {
        DateTime Date { get; }
        string DialledPhone { get; }
        int Duration { get; }
    }
}