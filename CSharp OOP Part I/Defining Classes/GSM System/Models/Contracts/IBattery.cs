﻿namespace GSM_System.Models.Contracts
{
    public interface IBattery
    {
        BatteryType? BatteryType { get; }
        double? HoursIdle { get; }
        double? HoursTalk { get; }
    }
}