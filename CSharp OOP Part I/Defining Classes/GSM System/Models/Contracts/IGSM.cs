﻿using System;
using System.Collections.Generic;

namespace GSM_System.Models.Contracts
{
    public interface IGSM
    {
        string Model { get; }
        string Manufacturer { get; }
        decimal? Price { get; }
        string Owner { get; }
        IBattery BatteryCharacteristics { get; }
        IDisplay DisplayCharacteristics { get; }
        string CallHistory { get; }
        void AddCall(DateTime dateTime, string dialledNumber, int duration);
        void DeleteCall(int callIndex);
        void ClearHistory();
        decimal TotalCallsPrice(decimal pricePerMinute);
    }
}