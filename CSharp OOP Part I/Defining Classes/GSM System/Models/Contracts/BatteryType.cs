﻿namespace GSM_System.Models.Contracts
{
    public enum BatteryType
    {
        Li_Ion,
        NiMH,
        NiCd,
        Unknown
    }
}