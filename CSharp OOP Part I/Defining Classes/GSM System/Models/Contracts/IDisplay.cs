﻿namespace GSM_System.Models.Contracts
{
    public interface IDisplay
    {
        double? Size { get; }
        int? NumberOfColors { get; }
    }
}