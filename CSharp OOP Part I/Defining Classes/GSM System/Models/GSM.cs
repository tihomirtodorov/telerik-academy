﻿using GSM_System.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace GSM_System.Models
{
    public class GSM : IGSM
    {
        private static readonly GSM iphone4S = new GSM("Iphone 4S", "Apple", 400);

        private string model;
        private string manufacturer;
        private decimal? price;
        private string owner;
        private IBattery batteryCharacteristics;
        private IDisplay displayCharacteristics;
        private List<Calls> callHistory;

        public GSM(string model, string manufacturer, decimal? price = null, string owner = null,
            IBattery battery = null, IDisplay display = null)
        {
            this.Model = model;
            this.Manufacturer = manufacturer;
            this.Price = price;
            this.Owner = owner;
            this.BatteryCharacteristics = battery;
            this.DisplayCharacteristics = display;
            this.callHistory = new List<Calls>();
        }

        public GSM Iphone4S
        {
            get { return iphone4S; }
        }

        public string Model
        {
            get { return model; }
            set { model = value; }
        }

        public string Manufacturer
        {
            get { return manufacturer; }
            set { manufacturer = value; }
        }

        public decimal? Price
        {
            get { return price; }
            set { price = value; }
        }

        public string Owner
        {
            get { return owner; }
            set { owner = value; }
        }

        public IBattery BatteryCharacteristics
        {
            get { return batteryCharacteristics; }
            set { batteryCharacteristics = value; }
        }

        public IDisplay DisplayCharacteristics
        {
            get { return displayCharacteristics; }
            set { displayCharacteristics = value; }
        }


        public string CallHistory
        {
            get
            {
                if (this.callHistory.Count == 0)
                {
                    return "Call history is empty";
                }

                var callsInfo = new StringBuilder();

                foreach (var call in this.callHistory)
                {
                    callsInfo.AppendLine(call.ToString());
                }

                return callsInfo.ToString().Trim();
            }
        }

        public void AddCall(DateTime dateTime, string dialledNumber, int duration)
        {
            this.callHistory.Add(new Calls(dateTime, dialledNumber, duration));
        }
        // Why the second one?
        public void AddCall(Calls call)
        {
            this.callHistory.Add(call);
        }

        // TODO Add validations
        public void DeleteCall(int callIndex)
        {
            this.callHistory.RemoveAt(callIndex - 1);
        }

        public void ClearHistory()
        {
            this.callHistory.Clear();
        }

        public decimal TotalCallsPrice(decimal pricePerMinute)
        {
            decimal totalPrice = 0;

            foreach (var call in this.callHistory)
            {
                int minutesCall = call.Duration / 60;
                int secondsCall = call.Duration % 60;

                totalPrice += minutesCall * pricePerMinute;

                if (secondsCall > 0)
                {
                    totalPrice += pricePerMinute;
                }
            }

            return totalPrice;
        }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Model: {this.Model}\n" +
                $"Manufacturer: {this.Manufacturer}\n" +
                $"Price: {this.Price}\n" +
                $"Owner: {this.Owner}\n" +
                $"Battery Characteristics: {this.BatteryCharacteristics.ToString()}\n" +
                $"Display Characteristics: {this.DisplayCharacteristics.ToString()}");

            return sb.ToString().TrimEnd();
        }
    }
}