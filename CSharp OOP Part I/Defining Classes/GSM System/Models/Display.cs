﻿using GSM_System.Models.Contracts;
using System.Text;

namespace GSM_System.Models
{
    public class Display : IDisplay
    {
        private double? size;
        private int? numberOfColors;

        public Display(double? size = null, int? numberOfColors = null)
        {
            this.Size = size;
            this.NumberOfColors = numberOfColors;
        }

        // TODO: add validation to properties
        public double? Size
        {
            get { return size; }
            set { size = value; }
        }

        public int? NumberOfColors
        {
            get { return numberOfColors; }
            set { numberOfColors = value; }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Size: {this.Size}, Number of colors: {this.numberOfColors}");

            return sb.ToString().TrimEnd();
        }
    }
}