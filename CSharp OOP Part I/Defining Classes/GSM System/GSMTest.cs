﻿using GSM_System.Models;
using GSM_System.Models.Contracts;
using System;
using System.Collections.Generic;

namespace GSM_System
{
    public static class GSMTest
    {
        public static void TestPhones()
        {
            // Create a list to save the phones;
            List<IGSM> phones = new List<IGSM>();

            // Create four phones;
            IGSM samsung = new GSM("Samsung S8", "Samsung", 1000, "Ivan",
                new Battery(BatteryType.Li_Ion, 200, 30),
                new Display(5.6, 1230000));
            IGSM huawei = new GSM("Huawei P20", "Huawei", 1200, "Gosho",
                new Battery(BatteryType.NiMH, 1000, 50),
                new Display(6.0, 532255325));
            IGSM xiaomi = new GSM("Xiaomi 123", "Xiaomi", 1400, "Chocho",
                new Battery(BatteryType.Unknown, 432, 111),
                new Display(7.2, 5435345));
            IGSM lg = new GSM("LG G7", "LG", 500, "Pesho",
                new Battery(BatteryType.NiCd, 111, 55),
                new Display(8.0, 234243));

            // Add the phones;
            phones.Add(samsung);
            phones.Add(huawei);
            phones.Add(xiaomi);
            phones.Add(lg);

            // Foreach throughout the phone list and ToString() all of them;
            foreach (var phone in phones)
            {
                Console.WriteLine(phone.ToString());
                Console.WriteLine();
            }
        }
    }
}